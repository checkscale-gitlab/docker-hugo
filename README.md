# sjugge/hugo

Runs the latest stable [Hugo](https://gohugo.io) static site generator on a [`fedora` base](https://hub.docker.com/_/fedora/), using [the Copr installer](https://copr.fedorainfracloud.org/coprs/daftaupe/hugo/). 

[![pipeline status](https://gitlab.com/sjugge/docker-hugo/badges/master/pipeline.svg)](https://gitlab.com/sjugge/docker-hugo/commits/master)


## Prerequisites

A working Hugo codebase in the `./app` directory.

````
config.toml
content/
static/
themes/
````

Refer to the [quickstart docs](http://gohugo.io/getting-started/quick-start/) or the [demo setup in my docker-playground](https://gitlab.com/sjugge/docker-playground/tree/master/Dockerfiles/hugo-demo).

## Usage

### Setup

* Add a working Hugo codebase in `./app`, see above.
* Create a `Dockerfile` and use this image as a base, i.e.:

````
FROM registry.gitlab.com/sjugge/docker-hugo
````

### Build

Build your image:

````
docker build -t hugo
````

### Run

````
docker run -dit -p 1313:1313 --mount type=bind,source=`pwd`/app,destination=/app hugo
````

This will run a container based of your newly created images and serve your Hugo site over [http://localhost:1313](http://localhost:1313). 

The `./app` directory will be shared between the host and container. Changes to code and content will be picked up by `hugo server` and should be available inmediatly.

### Logs

Tail the `hugo watch` output on a detached container:

````
docker logs -f `docker ps -q`
````

## Notes

This image was not created with Docker for Mac / Windows in mind. Additional tweaks may be required to access the served site.

## Contributing

If you see room for improvement, pull requests as well as feedback, are very welcome.

## Issues & feature requests

See the [repo's issue queue](https://gitlab.com/sjugge/docker-hugo/issues) for know issues and to report issues or request additional stuffs.

## Author

- Jurgen Verhasselt - https://gitlab.com/sjugge
- Source: https://gitlab.com/sjugge/docker-hugo

## License

[MIT License](https://gitlab.com/sjugge/docker-hugo/blob/master/LICENSE)
